const theTwoPI = Math.PI * 2;

const theCanvas = document.getElementById("canvas");
const theContext = theCanvas.getContext("2d");

const theWidth = theCanvas.width;
const theHeight = theCanvas.height;
const theDots = 400;
const theRadius = 70; // Уменьшил радиус, чтобы круг был виден

const theAngle = theTwoPI / theDots;

let theCof = 0.0001;

theContext.strokeStyle = "rgb(120, 241, 201)";

// Центрируем контекст по центру холста
theContext.translate(theWidth / 2, theHeight / 2);

const typeCoord = (theDotNum) => {
  const theX = Math.cos(theAngle * theDotNum) * theRadius;
  const theY = Math.sin(theAngle * theDotNum) * theRadius;
  return { theX, theY };
};

theContext.beginPath();
theContext.arc(0, 0, theRadius, 0, theTwoPI);
theContext.stroke();

const doFrame = () => {
  theContext.clearRect(-theWidth / 2, -theHeight / 2, theWidth, theHeight);

  theContext.beginPath();
  for (let theI = 0; theI < theDots; theI++) {
    const theDotX = theI;
    const theDotY = theDotX * theCof;

    const { theX: startX, theY: startY } = typeCoord(theDotX);
    const { theX: endX, theY: endY } = typeCoord(theDotY);

    const theGradient = theContext.createLinearGradient(
      startX,
      startY,
      startX + theRadius,
      startY + theRadius
    );

    theGradient.addColorStop(0, "rgb(120, 241, 201)");
    theGradient.addColorStop(1, "rgb(180, 241, 201)");

    theContext.strokeStyle = theGradient;

    theContext.moveTo(startX, startY);
    theContext.lineTo(endX, endY); // Исправлено lineto на lineTo
  }

  theContext.stroke();
  theCof += 0.01;
  requestAnimationFrame(doFrame);
};
doFrame();



// 

// var theTwoPI = Math.PI * 2;

// var theContext = document.getElementById("canvas").getContext("2d");

// var theWidth = 1000;
// var theHeight = 700;
// var theDots = 400;
// var theRadius = 300;

// var theAngle = theTwoPI / theDots;

// var theCof = 0.1;

// theContext.strokeStyle = "rgb(255, 0, 0)";

// var typeCoord = (theDoteNum) => {
//   var theX = Math.cos(theAngle * theDotNum) * theRadius;
//   var theY = Math.sin(theAngle * theDotNum) * theRadius;
//   return { theX, theY };
// };

// theContext.translate(300, 300);

// theContext.beginPath();
// theContext.arc(0, 0, 300, 0, theTwoPI);
// theContext.stroke();

// var doFrame = () => {
//   theContext.clearRect(-300, -300, theWidth, theHeight);

//   theContext.beginPath();
//   for (var theI = 0; theI < theDots; theI++) {
//     var theDotX = theI;
//     var theDotY = theDotX * theCof;

//     var { theX: startX, thrY: startY } = typeCoord(theDotX);
//     var { theX: endX, thrY: endY } = typeCoord(theDotY);

//     var theGradient = theContext.createLinearGradient(
//       startX,
//       startY,
//       startX + theRadius,
//       startY + theRadius
//     );

//     theGradient.addColorStop(0, "rgb(200,0,0)");
//     theGradient.addColorStop(1, "rgb(100,0,0)");

//     theContext.strokeStyle = theGradient;

//     theContext.moveTo(startX, startY);
//     theContext.lineto(endX, endY);
//   }

//   theContext.stroke();
//   theCof += 0.01;
//   requestAnimationFrame(doFrame);
// };
// doFrame();
